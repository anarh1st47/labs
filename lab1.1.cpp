#include <iostream>//namespace std
#include <cstdlib> //malloc
#include <cmath>   //abs

using namespace std;

int main(){
  bool first = 1, isSum = 0; // Первый ли раз встречается положительное число, нужно ли считать сумму
  float max = 0, sum = 0;    // Записываем максимальное значение(по модулю) и сумму
  int n, kolZeros=0;         // Количество чисел и число нулей
  cout<<"Введите N: ";
  cin>>n;
  float *p = (float*)malloc(n * sizeof(float));
  cout<<"Введите "<< n <<" цифр(ы)\n";
  for(int i = 0;i < n;i++){
    cin >> p[i];
    //find max
    if(abs(p[i])>max)  max = abs(p[i]);
    //find sum
    if(first && p[i] > 0)    {isSum = 1;first = 0;}
    else if(isSum)
      if(p[i]<=0)  sum+=p[i];
      else isSum = 0;
  }
  cout<<"Макимальное значение(по модулю): "<<max<<", сумма элементов, расположенных между 1 и 2 положительными элементами: "<<sum<<endl<<"Массив, с нулями в конце:\n";
  
  for(int i=0;i<n;i++){ // printing mas without zeroes, couning zeroes
    if(p[i]!=0) cout<< p[i]<<" ";
    else kolZeros++;
  }
  //printing zeros
  while(kolZeros>0){
     kolZeros--;cout<<"0 ";
  }
    
  cout<<endl<<"Программа завершилась успешно"<<endl;
  cin>>n;
  return 0;
}
