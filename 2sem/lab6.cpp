#include <iostream>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#define N 5
#define M 10

using namespace std;
using namespace boost::numeric::ublas;

int main(){
  matrix<double> m(N,M);
  int cnt = 0;
  for (unsigned i = 0; i < m.size2 (); ++ i)
        for (unsigned j = 0; j < m.size1 (); ++ j){
            m (j, i) = (5 * i + j)*0;
            if(m(j,i)==0){ cnt++; break;}
        }
  cout<<m<<endl<<cnt<<endl;


  return 0;
}
