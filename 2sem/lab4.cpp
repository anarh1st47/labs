#include <iostream>
/*Varianth 12*/
using namespace std;

class B{
  int a;
public:
  B(){}
  B(int x){a=x;}
  void show_B(){cout<<"B = "<<a<<endl;}
};
class D1 : public B{
	int b;
public:
  D1(int x, int y) :B(y) {b = x;	cout << "d1"<<endl;}
	void show_D1(){	cout << "D1=   " << b<< endl;	show_B();}
};

class D2 : private B{
	int c;
public:
  D2(int x, int y) :B(y) {c = x;	cout << "d2"<<endl;}
	void show_D2(){	cout << "D2=   " << c << endl;	show_B();}
};

class D3 : private B{
	int d;
public:
  D3(int x, int y) :B(y) {d = x;	cout << "d3"<<endl;}
	void show_D3(){	cout << "D3=   " << d << endl;	show_B();}
};
class D4 : private D1, public D2{
  int e;
public:
  D4(int x, int y, int z, int i, int j) : D1(y, z), D2(i, j){e=x;}
  void show_D4(){cout<<"D4 = "<<e<<endl;show_D1(); show_D2();}
};

class D5 : public D2, private D3{
  int f;
public:
  D5(int x, int y, int z, int i, int j) : D2(y, z), D3(i, j){f=x;}
  void show_D5(){cout<<"D5 = "<<f<<endl;show_D2(); show_D3();}
};

int main(){

	D4 temp(1,2,3,4,5);
	D5 templ(10, 11, 12, 13, 14);
	cout << "D4 temp(1,2,3,4,5);\n";
	cout << "D5 templ(10, 11, 12, 13, 14);\n";
	cout << "D4 line\n";
	temp.show_D4();
	cout << "D5 line\n";
	templ.show_D5();
	cout << "sizeof: " <<sizeof(B) << endl;
	return 0;
}
