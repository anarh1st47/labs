#include <iostream>
/*Varianth 2*/
using namespace std;
class Pair{
protected:
  int x, y;
public:
  Pair(int _x=0, int _y=0){this->x=_x; this->y=_y;}
  int getX(){return this->x;}
  int getY(){return this->y;}
  void setX(int _x){this->x = _x;}
  void setY(int _y){this->y = _y;}
  void set(int _x, int _y){this->x = _x; this->y = _y;}
  Pair product(Pair &obj){return Pair(this->x*obj.x, this->y*obj.y);}
};
class Rect: public Pair{
public:
  Rect(int length, int height) : Pair(length, height){};
  int area(){return this->x*this->y;}
  int perimeter(){return 2*(this->x+this->y);}
};
int main(){
  /*Pair example*/
  Pair kek, lel;
  kek.set(100, 500);
  lel.set(101, 520);
  Pair t = kek.product(lel);
  cout<<t.getX()<<" "<<t.getY()<<endl;

  /*Rect example*/
  Rect keks(5, 10);
  cout<<keks.area()<<" "<<keks.perimeter()<<endl;
  return 0;
}
