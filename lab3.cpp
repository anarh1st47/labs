#include <iostream>//namespace std
#include <cmath>   //abs
#include <limits>  //maxfloat
#include <fstream> //input file
#define maxfloat numeric_limits<float>::max()
#define cin fin

using namespace std;

ifstream fin;

int main(){
  fin.open("kurwa");
  int count = 0, sum = 0;
  float mas[12][12];
  for(int i = 0; i < 12; i++) for(int j = 0; j < 12; j++) 
    if(i == 0 || j == 0 || i == 11 || j == 11) //Крайние клетки забиваем макс. значениями
      mas[i][j] = maxfloat;
    else
      cin>>mas[i][j]; //Считываем матрицу 10*10, оставляя по одному элементу с каждой стороны, чтобы без исключений считать крайние значения
  //Вычисляем количество минимумов 
  for(int i = 1; i < 11; i++) for(int j = 1; j < 11; j++)
    if(mas[i][j]<mas[i-1][j] && mas[i][j]<mas[i][j-1] && mas[i][j]<mas[i+1][j] && mas[i][j]<mas[i][j+1])
      if(mas[i][j]<mas[i-1][j-1] && mas[i][j]<mas[i+1][j-1] && mas[i][j]<mas[i+1][j-1] && mas[i][j]<mas[i+1][j+1]) // Если соседи--квадрат. Иначе закомментировать
         {count++;cout<<mas[i][j]<<endl;}
  //Вычисляем сумму элементов, находящихся выше главной диагонали
  for(int i = 1; i < 11; i++) for(int j = 1; j < 11; j++)
    if(j > i)      sum+=abs(mas[i][j]); //Выше главной диагонали
  cout<<"Сумма:\t\t"<<sum<<endl<<"Количество:\t"<<count<<endl;
  
  for(int i = 0; i < 12; i++){
   for(int j = 0; j < 12; j++) 
    if(mas[i][j]!=maxfloat)
     cout<<mas[i][j]<<"\t";
    else
     cout<<"max"<<"\t";
   cout<<endl;
  }
  return 0;
}
