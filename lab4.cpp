#include <iostream>
#include <cstring>
#include <fstream>
#include <regex> //-std=c++11
#define  dot  <<'.'<<

const auto numberOfUsers = 100;
using namespace std;
ifstream fin;

struct note{
    string surname, name, phone;
    int date[3];
};


int check(struct note leshiy){ // Чекер дат
  int max=0; 
  if(leshiy.date[0]<1||leshiy.date[1]<1) // положительные даты онли
    return 1;
  
  if((leshiy.date[1]%2 == 1 && leshiy.date[1] < 8) || (leshiy.date[1]%2 == 0 && leshiy.date[1] >= 8)) // Максимальное количество дней в данном месяце
    max = 31;
  else if(leshiy.date[1] == 2 && leshiy.date[2]%4 != 0)
    max = 28;
  else if(leshiy.date[1] == 2 && leshiy.date[2]%4 == 0)
    max = 29;
  else
    max = 30;
  
  if(leshiy.date[0] > max || leshiy.date[1] > 12 || leshiy.date[2] > 2015)
    return 1;
  return 0;
}

void print(struct note items){  // Вывод ээлементов структуры
  cout<<items.surname<<"\t"<<items.name<<"\t"<<items.phone<< "\t"<<(char*)(check(items) ? " invalid date" : " valid date")  <<"\t"<< items.date[0] dot items.date[1] dot items.date[2]<<endl;
}

note *qs_struct(struct note items[], int left, int right){ //Бытсрая сортировка структур
  int i, j;
  string x = items[(left+right)/2].phone;
  struct note temp;
  i = left; j = right;

  do{
    while( (items[i].phone.compare(x) < 0) && (i < right)) i++; 
    while( (items[j].phone.compare(x) > 0) && (j > left) ) j--; 
    if(i <= j){
      temp = items[i];
      items[i] = items[j];
      items[j] = temp;
      i++; j--;
    }
  } while(i <= j);

  if(left < j)    qs_struct(items, left, j);
  if(i < right)   qs_struct(items, i, right);
  return items;
}

void find(struct note items[]){ //Поиск по номеру
  string nidPhone;
  cout<<"enter phone(can use * and ?): ";
  cin>>nidPhone;
  
  /*
   *Заменяем нужные нам выражения на выражения, нужные компилятору
   *Пилим регулярное выражение.
   */
  //TODOne: Запилить это в цикле
  auto pos = 0;
  for(auto i = 0; i < nidPhone.length(); i++){
    size_t found=nidPhone.find_first_not_of("1234567890.", pos); 
    if(nidPhone[found]=='?')
      nidPhone.replace(found,1,".");  // Заменяем ? на .
    else if(found!=string::npos){
      nidPhone.replace(found,0,"."); // Заменяем * на .* 
    pos = found + 2;
    }
  }
  nidPhone ="^" + nidPhone + "$"; //добавляем к регулярке начало и конец строки, чтобы не выдавало лишних значений.
  
  cout<<"Регулярка: "<<nidPhone<<endl;
  smatch m;
  regex e (nidPhone);
  for(auto i = 0; i < numberOfUsers; i++)
    if (regex_search (items[i].phone,m,e))
      print(items[i]); // Вывод найденных юзеров
}


int main(){
  fin.open("lab4all.kurwa");
  note user[numberOfUsers];
  for(auto i = 0; i < numberOfUsers; i++){
    fin>>user[i].surname>>user[i].name>>user[i].phone>>user[i].date[0]>>user[i].date[1]>>user[i].date[2];
  }
  note *items = qs_struct(user,0,numberOfUsers-1);
  for(auto i = 0; i < numberOfUsers; i++)
    print(items[i]);
  find(items);
  system("read -p '\n\n\033[92mПрограмма завершилась успешно.\033[0m'");
  return 0;
}
